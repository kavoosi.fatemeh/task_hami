package com.taskhami.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.stealthcopter.networktools.PortScan
import com.taskhami.data.*
import com.taskhami.repository.local.ItemDao
import com.taskhami.repository.local.MyPreferences
import com.taskhami.repository.remote.MyApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val myApi: MyApi,
    private val dao: ItemDao,
    private val myPreferences: MyPreferences
) : Repository {
    companion object {
        const val TAG = "RepositoryImpl"
    }

    override suspend fun onRegisterDevice(registerBody: RegisterBody): Boolean {
        if (myPreferences.getToken() == "" )  {
            try {
                myApi.onRegister(registerBody).apply {
                    when {
                        isSuccessful ->
                            this.body()?.let {
                                it.token?.let { it1 -> myPreferences.setToken(it1) }
                                return true
                            }
                        else ->
                            return false

                    }

                }
            } catch (e: Exception) {
                return false

            }
        }
        return true
    }

    override fun isRegisteredToken(): Boolean = !myPreferences.getToken().isNullOrBlank()


    override fun onGetItemsFromCache(): LiveData<List<ItemEntity>> {
        return dao.getItems()

    }

    override suspend fun onGetPortList(): ResultState<List<Item>> =
        withContext(Dispatchers.IO) {

            try {
                myApi.onGetPortList().let {
                    when {
                        it.isSuccessful -> {
                            //1- cach items to db
                            cacheItems(it.body()?.let { it1 -> Mapper.mapItemsToItemsEntity(it1) })
                            //2- check for status connection ports in items
                            checkStatusConnection(it.body())

                            ResultState.Success(it.body())

                        }
                        else -> ResultState.Error(it.errorBody().toString())
                    }

                }
            } catch (e: Exception) {
                ResultState.Error(e.message.toString())
            }
        }

    private fun cacheItems(items: List<ItemEntity>?) {
        items?.let { dao.insertItems(it) }

    }

    override suspend fun onRegisterInspectedServer(inspectServerBody: InspectServerBody) {
        myApi.onRegisterInspectedServer(inspectServerBody).apply {
            when {
                isSuccessful ->
                    body()?.let {
                        Log.e(
                            TAG,
                            "onRegisterInspectedServer: $it"
                        )

                    }
                else ->
                    Log.e("p0orerror", "register inspectedbody ${errorBody()}")
            }
        }

    }


    private suspend fun checkStatusConnection(items: List<Item>?) {
        items?.forEach foreachItem@{ item ->
            checkStatusPort(Mapper.mapItemToItemEntity(item))
        }

    }

    private suspend fun checkStatusPort(item: ItemEntity) {
        item.ports.forEach foreachPort@{ port ->
            PortScan.onAddress(item.ip).setMethodUDP().setPort(port.toInt()).doScan()
                .isNotEmpty()
                .let {
                    item.isActive = true

                    //get status of port and update status of it on db
                    onUpdateItemStatus(item)

                    //register status connection port to server
                    onRegisterInspectedServer(
                        InspectServerBody(
                            item.id,
                            item.hashKey,
                            item.ip,
                            item.isActive,
                            "isp"
                        )
                    )
                    return

                }


        }

    }


    private fun onUpdateItemStatus(item: ItemEntity) {
        dao.onUpdateStatusPort(item)
    }

}