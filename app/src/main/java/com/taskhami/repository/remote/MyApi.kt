package com.taskhami.repository.remote


import com.taskhami.data.InspectServerBody
import com.taskhami.data.Item
import com.taskhami.data.RegisterBody
import com.taskhami.data.Token
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface MyApi {

    @POST("inspectors/register/")
    suspend fun onRegister(@Body registerBody: RegisterBody): Response<Token>

    @GET("inspectors/inquiry-server/")
    suspend fun onGetPortList(): Response<List<Item>>

    @POST("inspectors/inspected-server/")
    suspend fun onRegisterInspectedServer(@Body inspectServerBody: InspectServerBody): Response<InspectServerBody>
}