package com.taskhami.repository.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.taskhami.data.Converters
import com.taskhami.data.ItemEntity

@Database(entities = [ItemEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun itemDao(): ItemDao

    companion object {

        fun getInstance(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "AppDatabase"
        ).build()

    }
}