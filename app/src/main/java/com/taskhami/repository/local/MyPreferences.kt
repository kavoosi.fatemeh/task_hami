package com.taskhami.repository.local

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MyPreferences @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private val keyToken = "TOKEN" //token jwt


    fun setToken(token: String) {
        sharedPreferences.edit().putString(keyToken, token).apply()
    }

    fun getToken() = sharedPreferences.getString(keyToken, "")

}