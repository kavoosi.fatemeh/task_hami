package com.taskhami.repository.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.taskhami.data.ItemEntity

@Dao
interface ItemDao {

    @Query("SELECT * FROM ItemEntity ")
    fun getItems(): LiveData<List<ItemEntity>>

   @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertItems(items: List<ItemEntity>)

    @Update
    fun onUpdateStatusPort(item: ItemEntity)

}