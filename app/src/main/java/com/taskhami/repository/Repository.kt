package com.taskhami.repository

import androidx.lifecycle.LiveData
import com.taskhami.data.*

interface Repository {

    suspend fun onRegisterDevice(registerBody: RegisterBody): Boolean
    suspend fun onGetPortList(): ResultState<List<Item>>
    fun onGetItemsFromCache(): LiveData<List<ItemEntity>>
    suspend fun onRegisterInspectedServer(inspectServerBody: InspectServerBody)
    fun isRegisteredToken(): Boolean
}