package com.taskhami.di

import com.taskhami.repository.Repository
import com.taskhami.repository.RepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
abstract class BindingModule {

    @Singleton
    @Binds
    abstract fun bindingVenueRepository(repositoryImpl: RepositoryImpl): Repository
}