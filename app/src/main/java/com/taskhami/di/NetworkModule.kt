package com.taskhami.di

import com.google.gson.GsonBuilder
import com.taskhami.Const
import com.taskhami.repository.local.MyPreferences
import com.taskhami.repository.remote.MyApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object NetworkModule {

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor() =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

    @Provides
    @Singleton
    fun provideOkHttp(
        loggerInterceptor: HttpLoggingInterceptor,
        @Named("tokenInterceptor") authInterceptor: Interceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggerInterceptor)
            .addInterceptor(authInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Const.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    @Named("tokenInterceptor")
    fun provideTokenInterceptor(myPreferences: MyPreferences): Interceptor {
        return Interceptor { chain ->
            val req = chain.request()

            val request = req.newBuilder()
                // .addHeader("Authorization", "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpbnNwZWN0b3JfaWQiOjIyLCJleHAiOjE5MjkwMDQ3ODd9.PWrLJ74uUP5IWpk9Yd-XRO3D1WswH2T5F_ysCZAXcE4")
                .addHeader("Authorization", "JWT ${myPreferences.getToken()}")
                .build()

            chain.proceed(request)
        }
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): MyApi = retrofit.create(MyApi::class.java)

}