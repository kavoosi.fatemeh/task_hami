package com.taskhami.ui.port_list

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.taskhami.data.Item
import com.taskhami.data.RegisterBody
import com.taskhami.data.ResultState
import com.taskhami.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PortListViewModel @ViewModelInject constructor(private val repository: Repository) :
    ViewModel() {

    val itemsMediatorLiveData = repository.onGetItemsFromCache()


    val isRegisteredDevice = MutableLiveData<Boolean>()
    val isReadyToken = MutableLiveData<Boolean>()

    init {
        isRegisteredDevice.value = repository.isRegisteredToken()
    }

    val resultState by lazy {
        MutableLiveData<ResultState<List<Item>>>()
    }

    fun onRegisterDevice(registerBody: RegisterBody) {
        resultState.value=ResultState.Loading(true)
        viewModelScope.launch(Dispatchers.IO) {
            repository.onRegisterDevice(RegisterBody(registerBody.name, registerBody.code)).let {
                isReadyToken.postValue(it)
                resultState.postValue(ResultState.Loading(false))

            }
        }
    }

    fun onGetPortList() {
        resultState.value = ResultState.Loading(true)
        viewModelScope.launch {
            repository.onGetPortList().apply {
                resultState.value = this
                resultState.value = ResultState.Loading(false)
            }


        }
    }

}
