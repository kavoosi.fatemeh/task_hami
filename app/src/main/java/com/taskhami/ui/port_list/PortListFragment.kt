package com.taskhami.ui.port_list

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.taskhami.Const
import com.taskhami.app.databinding.PortListFragmentBinding
import com.taskhami.data.RegisterBody
import com.taskhami.data.ResultState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PortListFragment : Fragment() {

    private var _binding: PortListFragmentBinding? = null

    // This property is only valid between onCreateView and  onDestroyView
    private val binding get() = _binding!!

    private var portAdapter = PortAdapter()


    private val viewModel: PortListViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PortListFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        initRecycler()
        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //register device
        binding.btnRegisterOtt.setOnClickListener {
            if (binding.edtOtt.text.isEmpty())
                showToast("توکن را وارد نمایید.")
            else viewModel.onRegisterDevice(RegisterBody(Const.NAME_REG, binding.edtOtt.text.toString()))
        }

        //----------------------------------observes

        viewModel.isRegisteredDevice.observe(viewLifecycleOwner, {
            if (it) //isRegistered
                viewModel.onGetPortList()
            else //is Not registered and show view for register
                showOttView()
        })

        viewModel.isReadyToken.observe(viewLifecycleOwner, {
            if (it) {
                //token is ready and ready for get items
                hiddenOttView()
                viewModel.onGetPortList()
            } else {
                showOttView()
                showToast("مشکلی در توکن رخ داده ")
            }
        })


        viewModel.itemsMediatorLiveData.observe(viewLifecycleOwner,
            {
                if (it.isNotEmpty()) {
                    portAdapter.list = it
                }
            })


        viewModel.resultState.observe(viewLifecycleOwner,
            {
                when (it) {
                    is ResultState.Loading ->
                        showLoading(it.isShow)

                    is ResultState.Success ->
                        Log.d(
                            PortListFragment::class.java.simpleName,
                            "observe result port Success: ${it.data}"
                        )
                    is ResultState.Error -> {
                        showLoading(false)
                        showToast(it.e)
                    }

                }
            })

    }

    private fun showLoading(isShow: Boolean) {
        if (isShow)
            binding.pbLoading.visibility = View.VISIBLE
        else
            binding.pbLoading.visibility = View.INVISIBLE

    }

    private fun showOttView() {
        binding.edtOtt.visibility = View.VISIBLE
        binding.btnRegisterOtt.visibility = View.VISIBLE
    }

    private fun hiddenOttView() {
        binding.edtOtt.visibility = View.INVISIBLE
        binding.btnRegisterOtt.visibility = View.INVISIBLE
    }

    private fun initRecycler() {
        val rv = binding.rvVenue
        rv.layoutManager = LinearLayoutManager(
            requireContext(), RecyclerView.VERTICAL,
            false
        )
        rv.adapter = portAdapter

    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).apply {
            setGravity(Gravity.CENTER, 0, 0)
            show()
        }
    }
}

