package com.taskhami.ui.port_list

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.taskhami.app.R
import com.taskhami.data.ItemEntity

class PortAdapter : RecyclerView.Adapter<PortAdapter.PortViewHolder>() {
    var list = listOf<ItemEntity>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PortViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_port, parent, false)
        return PortViewHolder(view)
    }

    override fun onBindViewHolder(holder: PortViewHolder, position: Int) {
        holder.bind(list[position])

    }

    //__viewwHolder _______________________________________________________________________________

    class PortViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvId: TextView = view.findViewById(R.id.tv_id)
        private val tvIp: TextView = view.findViewById(R.id.tv_ip)
        private val tvPort: TextView = view.findViewById(R.id.tv_port)
        private val tvIsActive: TextView = view.findViewById(R.id.tv_is_active)


        fun bind(item: ItemEntity) {
            item.apply {
                "id: $id".also { tvId.text = it }
                tvIp.text = ip
                "port: $ports".also { tvPort.text = it }

                if (!isActive)
                    tvIsActive.setTextColor(Color.parseColor("#ff0000"))
                else
                    tvIsActive.setTextColor(Color.parseColor("#28b565"))

                "status: $isActive".also { tvIsActive.text = it }
            }
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }
}