package com.taskhami.data

object Mapper {
    fun mapItemsToItemsEntity(items: List<Item>): List<ItemEntity> = items.map { item ->
        ItemEntity(
            item.id,
            item.hashKey,
            item.ip,
            item.ports,
            item.connectionStatus,
            item.isActive

        )
    }

    fun mapItemToItemEntity(item: Item): ItemEntity =
        ItemEntity(
            item.id,
            item.hashKey,
            item.ip,
            item.ports,
            item.connectionStatus,
            item.isActive

        )


}