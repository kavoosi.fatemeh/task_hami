package com.taskhami.data

data class RegisterBody(
    val name: String,
    val code: String
)