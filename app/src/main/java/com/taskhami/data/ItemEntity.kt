package com.taskhami.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ItemEntity(
    @PrimaryKey
    val id: Int,
    val hashKey: String,
    val ip: String,
    val ports: List<String>,
    val connectionStatus: String,
    var isActive: Boolean = false,
    var receivedIsp: String = "isp"
)