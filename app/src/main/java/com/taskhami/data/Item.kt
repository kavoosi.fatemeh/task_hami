package com.taskhami.data

import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("id")
    val id: Int,
    @SerializedName("hash_key")
    val hashKey: String,
    @SerializedName("ip")
    val ip: String,
    @SerializedName("ports")
    val ports: List<String>,
    @SerializedName("connection_status")
    val connectionStatus: String,


    @Transient var isActive: Boolean = false
)