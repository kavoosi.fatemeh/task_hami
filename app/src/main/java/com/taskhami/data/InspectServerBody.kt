package com.taskhami.data

import com.google.gson.annotations.SerializedName

data class InspectServerBody(
    @SerializedName("server")
    val id: Int,
    @SerializedName("hash_key")
    val hashKey: String,
    @SerializedName("ip")
    val ip: String,
    @SerializedName("is_active")
    val isActive: Boolean,
    @SerializedName("received_isp")
    val receivedIsp: String = "isp"
)